package com.demo.btreceiverhcard;

import java.lang.ref.WeakReference;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.btreceiverhcard.BTConnectionService.BTConnectionBinder;
import com.demo.btreceiverhcard.BTConnectionService.State;

public class ConnectActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connect);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new ConnectFragment()).commit();
		}
	}

	/**
	 * A fragment that fires up {@link BTConnectionService} and connects to the remote BT module.
	 */
	public static class ConnectFragment extends Fragment {

		private static final int REQUEST_ENABLE_BT = 1;

		private static final String LOG_TAG = "ConnectFragment";
		
		/** MAC address of our specific BT transceiver chip. */
		private static final String MAC_HCARD = "00:14:01:21:27:57";
		
		private TextView statusText;
		
		private BluetoothAdapter btAdapter;
		private Messenger messenger;
		private static final int MAX_RETRIES = 2;
		private int numRetries = 0;
		
		public ConnectFragment() {
			btAdapter = BluetoothAdapter.getDefaultAdapter();
			messenger = new Messenger(new ConnectingBTHandler(this));
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_connect, container, false);
			statusText = (TextView) rootView.findViewById(R.id.connectionStatusText);
			return rootView;
		}
		
		@Override
		public void onStart(){
			super.onStart();
			
			if(null == btAdapter){
				Toast.makeText(getActivity(), "Sorry, but your device does not have a Bluetooth antenna!", Toast.LENGTH_LONG).show();
				getActivity().finish();
				return;
			}
			if(!btAdapter.isEnabled()){
				// Prompt BT activation on phone
				Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			} else {
				initAndBindConnectionService();
			}
		}
		
		@Override
		public void onStop(){
			super.onStop();
			
			if(bound){
				btService.unregisterListener(messenger);
				Log.d(LOG_TAG, "Unbinding from service, service state: " + btService.getState());
				getActivity().getApplicationContext().unbindService(btTemporaryServiceConnection);
				bound = false;
			}
		}
		
		@Override
		public void onActivityResult(int requestCode, int resultCode, Intent data){
			super.onActivityResult(requestCode, resultCode, data);
			if(REQUEST_ENABLE_BT == requestCode){
				if(Activity.RESULT_OK == resultCode){
					initAndBindConnectionService();
				} else {
					Toast.makeText(getActivity(), "Bluetooth is necessary to continue.", Toast.LENGTH_LONG).show();
					getActivity().finish();
					// TODO offer button to ask again?
				}
			}
		}
		
		/** Initializes (if it has not yet been initialized) and binds to the {@link BTConnectionService} to query its status. */
		private void initAndBindConnectionService(){
			getActivity().getApplicationContext().startService(BTConnectionService.getIntent(getActivity(), messenger));
			Intent intent = new Intent(getActivity(), BTConnectionService.class);
			getActivity().getApplicationContext().bindService(intent, btTemporaryServiceConnection, Context.BIND_AUTO_CREATE);
		}
		
		private void connectToDevice(String MAC){
			Log.d(LOG_TAG, "Attempting to connect to device with MAC address " + MAC);

			// Create a BT device which represents the remote BT chip (e.g. connected to the Arduino)
			BluetoothDevice btChip = btAdapter.getRemoteDevice(MAC);
			
			if(null == btChip){
				// Failed to connect!
				Toast.makeText(getActivity(), "Houston, we failed to retrieve the remote device.", Toast.LENGTH_SHORT).show();
				return;
			}
			
			btService.connect(btChip);
		}
		
		/** Continue to the data display activity once the connection has been established. */
		private void onConnected(){
			Log.d(LOG_TAG, "Connection done (?), moving on!");
			Toast.makeText(getActivity(), "Connected to FARM!", Toast.LENGTH_SHORT).show();
			Intent measureIntent = new Intent(getActivity(), MeasurementActivity.class);
			startActivity(measureIntent);
		}
		
		private BTConnectionService btService;
		private boolean bound = false;
		
		/** A temporary service connection to get access to its public methods while connecting to the remote. */
		private ServiceConnection btTemporaryServiceConnection = new ServiceConnection(){
			@Override
			public void onServiceConnected(ComponentName className, IBinder service) {
				BTConnectionBinder binder = (BTConnectionBinder) service;
				btService = binder.getService();
				bound = true;
				Log.d(LOG_TAG, "Connected to service, service state: " + btService.getState());
				
				switch(btService.getState()){
				case DISCONNECTED:
					statusText.setText("Disconnected.");
					connectToDevice(MAC_HCARD);
					break;
				case CONNECTED:
					statusText.setText("Connected!");
					onConnected();
					break;
				case CONNECTING:
					statusText.setText("Connecting...");
					// don't do anything, just wait; will handle in ConnectingBTHandler upon next state change
					break;
				}
			}

			@Override
			public void onServiceDisconnected(ComponentName className) {
				bound = false;
				Log.d(LOG_TAG, "Disconnected from service, service state: " + btService.getState());
			}
		};
		
		private static class ConnectingBTHandler extends Handler{
			/** WeakReference to avoid any potential memory leaks. */
			private WeakReference<ConnectFragment> fragment;
			
			public ConnectingBTHandler(ConnectFragment connectFragment){
				fragment = new WeakReference<ConnectFragment>(connectFragment);
			}

			@Override 
			public void handleMessage(Message msg){
				ConnectFragment connectFragment = fragment.get();
				if(null == connectFragment){
					Log.e(LOG_TAG, "ConnectingBTHandler reference to ConnectFragment is null, cannot handle message!");
					return;
				}
				switch(msg.what){
				case BTConnectionService.MESSAGE_STATE_CHANGE:
					State newState = (State) msg.obj;
					switch(newState){
					case DISCONNECTED:
						connectFragment.statusText.setText("Disconnected.");
						connectFragment.numRetries++;
						if(connectFragment.numRetries > MAX_RETRIES){
							Toast.makeText(connectFragment.getActivity(), "Exceeded maximum retries, try again later.", Toast.LENGTH_LONG).show();
							connectFragment.getActivity().getApplicationContext().stopService(new Intent(connectFragment.getActivity(), BTConnectionService.class));
							connectFragment.getActivity().finish();
						} else {
							Toast.makeText(connectFragment.getActivity(), "Failed to connect. Retrying, try " + connectFragment.numRetries + " out of " + MAX_RETRIES, Toast.LENGTH_SHORT).show();
							connectFragment.connectToDevice(MAC_HCARD);
						}
						break;
					case CONNECTING:
						connectFragment.statusText.setText("Connecting...");
						Log.d(LOG_TAG, "Service tells me it's connecting..");
						break;
					case CONNECTED:
						connectFragment.statusText.setText("Connected!");
						// hurrah, move forward!
						connectFragment.onConnected();
						break;
					}
					break;
				}
			}
		}
	}

}
