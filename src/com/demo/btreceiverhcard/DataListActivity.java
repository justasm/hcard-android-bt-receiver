package com.demo.btreceiverhcard;

import java.io.File;
import java.io.FilenameFilter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DataListActivity extends ActionBarActivity {

	private static final String LOG_TAG = "DataListActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_data_list);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new DataListFragment()).commit();
		}
	}

	/**
	 * A fragment which displays a list of past measurements.
	 */
	public static class DataListFragment extends ListFragment {

		private MeasurementAdapter adapter;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_data_list,
					container, false);
			return rootView;
		}
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState){
			super.onActivityCreated(savedInstanceState);
			
			// Retrieve all CSV files in our directory
			File storageDir = getDataFileStorageDir("HCARD");
			String[] fileList = storageDir.list(new FilenameFilter(){
				@Override
				public boolean accept(File dir, String filename) {
					return filename.endsWith(".csv");
				}
			});

			adapter = new MeasurementAdapter(getActivity(), fileList);
			setListAdapter(adapter);
		}
		
		@Override
		public void onListItemClick(ListView l, View v, int position, long id){
			Log.d(LOG_TAG, "Want to open file " + adapter.fileNameList[position]);
			
			Intent reviewIntent = new Intent(getActivity(), PastMeasurementActivity.class);
			reviewIntent.putExtra(PastMeasurementActivity.FILENAME_EXTRA, adapter.fileNameList[position]);
			startActivity(reviewIntent);
		}
		
		public File getDataFileStorageDir(String dataName){
			File file = getFile(dataName);
			if(!file.mkdirs() && !file.isDirectory()){
				Log.e(LOG_TAG, "Directory " + dataName + " not created.");
			}
			return file;
		}

		/* Checks if external storage is available for read and write */
		public static boolean isExternalStorageWritable() {
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				return true;
			}
			return false;
		}

		/* Checks if external storage is available to at least read */
		public static boolean isExternalStorageReadable() {
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state) ||
					Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
				return true;
			}
			return false;
		}

		void deleteExternalStoragePrivateFile(String dataName) {
			// Get path for the file on external storage.  If external
			// storage is not currently mounted this will fail.
			File file = getFile(dataName);
			if (file != null) {
				file.delete();
			}
		}

		boolean hasExternalStoragePrivateFile(String dataName) {
		    // Get path for the file on external storage.  If external
		    // storage is not currently mounted this will fail.
			File file = getFile(dataName);
		    if (file != null) {
		        return file.exists();
		    }
		    return false;
		}

		@TargetApi(Build.VERSION_CODES.KITKAT)
		private File getFile(String dataName){
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
				return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), dataName);
			} else {
				return new File(getActivity().getExternalFilesDir(null), dataName);
			}
		}
		
		/** Stores list of all the available filenames. */
		private class MeasurementAdapter extends BaseAdapter {

			private Context context;
			private String[] fileNameList;
			
			public MeasurementAdapter(Context context, String[] fileNameList){
				this.context = context;
				this.fileNameList = fileNameList;
			}
			
			@Override
			public int getCount() {
				return fileNameList.length;
			}

			@Override
			public Object getItem(int position) {
				return null;
			}

			@Override
			public long getItemId(int position) {
				return 0;
			}

			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				if(null == convertView){
					convertView = View.inflate(context, R.layout.item_list_file, null);
				}
				TextView name = (TextView) convertView.findViewById(R.id.name);
				name.setText(fileNameList[position]);
				return convertView;
			}
			
		}
	}
	
	

}
