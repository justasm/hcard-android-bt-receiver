package com.demo.btreceiverhcard;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.demo.btreceiverhcard.dataview.DataSeries;
import com.demo.btreceiverhcard.dataview.LineChart;

import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.TextView;
import android.os.Build;

/** Container for a {@link ReviewFragment}. */
public class PastMeasurementActivity extends ActionBarActivity {
	
	public static final String FILENAME_EXTRA = "filename";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_past_measurement);

		if (savedInstanceState == null) {
			Fragment frag = new ReviewFragment();
			Bundle args = new Bundle();
			args.putString(FILENAME_EXTRA, getIntent().getExtras().getString(FILENAME_EXTRA));
			frag.setArguments(args);
			getSupportFragmentManager().beginTransaction().add(R.id.container, frag).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.past_measurement, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	/**
	 * The review data display view. Displays the full trace of all potentiometer readings for a {@link Farm} previously saved to disk.
	 */
	public static class ReviewFragment extends Fragment {
		
		private static final String LOG_TAG = "ReviewFragment";

		private String fileName;
		private Farm farm;
		
		private TextView statusText;
		private TextView targetText;
		private LineChart graph;
		private List<DataSeries> activeSeries;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_measurement, container, false);
			statusText = (TextView) rootView.findViewById(R.id.currentState);
			targetText = (TextView) rootView.findViewById(R.id.currentTarget);
			
			statusText.setVisibility(View.GONE);
			targetText.setVisibility(View.GONE);
			
			graph = (LineChart) rootView.findViewById(R.id.graph);
			activeSeries = new ArrayList<DataSeries>(4);
			
			Bundle args = getArguments();
			if(null != args){
				fileName = args.getString(FILENAME_EXTRA);
				if(isExternalStorageReadable()){
					File dir = getDataFileStorageDir("HCARD");
					try {
						farm = new Farm(new File(dir, fileName));
						for(int i = 0; i < 4; i++){
							DataSeries series = farm.getData(i);
							graph.addSeries(series);
							activeSeries.add(series);
						}
					} catch (IOException e) {
						Log.e(LOG_TAG, "Failed to write to file.", e);
					}
					
				}
				
				setHasOptionsMenu(true);
				getActivity().getActionBar().setTitle(fileName);
			}
			return rootView;
		}
		
		@Override
		public boolean onOptionsItemSelected(MenuItem item){
			switch(item.getItemId()){
			case R.id.action_share:
				final Intent shareIntent = new Intent(Intent.ACTION_SEND);
				shareIntent.setType(MimeTypeMap.getSingleton().getMimeTypeFromExtension("csv"));
				shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(getDataFileStorageDir("HCARD"), fileName)));
				final Intent chooserIntent = Intent.createChooser(shareIntent, "Share your data as a CSV file.");
				startActivity(chooserIntent);
				return true;
			}
			return super.onOptionsItemSelected(item);
		}
		
		/* Checks if external storage is available to at least read */
		public static boolean isExternalStorageReadable() {
			String state = Environment.getExternalStorageState();
			if (Environment.MEDIA_MOUNTED.equals(state) ||
					Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
				return true;
			}
			return false;
		}
		
		@TargetApi(Build.VERSION_CODES.KITKAT)
		private File getFile(String dataName){
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
				return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), dataName);
			} else {
				return new File(getActivity().getExternalFilesDir(null), dataName);
			}
		}
		
		public File getDataFileStorageDir(String dataName){
			File file = getFile(dataName);
			if(!file.mkdirs() && !file.isDirectory()){
				Log.e(LOG_TAG, "Directory " + dataName + " not created.");
			}
			return file;
		}
	}

}
