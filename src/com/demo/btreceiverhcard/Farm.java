package com.demo.btreceiverhcard;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.graphics.Color;
import android.util.Log;

import com.demo.btreceiverhcard.dataview.DataSeries;
import com.demo.btreceiverhcard.dataview.DataType;
import com.demo.btreceiverhcard.dataview.SimpleSeries;

/** A model representing the game board's state, including a log of all received data. */
public class Farm {
	private static final int NUM_SINKS = 4;
	private static final String[] NAMES = {"Sheep", "Pig", "Horse", "Cow"};
	private static final String[] POT_NAMES;
	private static final int[] DATA_COLORS = {Color.parseColor("#C7007D"), Color.parseColor("#1047AA"), Color.parseColor("#FFA500"), Color.parseColor("#A8F000")};
	private static final int[] FILL_COLORS = {Color.parseColor("#E366B5"), Color.parseColor("#6B90D4"), Color.parseColor("#FFCE73"), Color.parseColor("#CFF76F")};
	static{
		for(int i = 0; i < FILL_COLORS.length; i++){
			FILL_COLORS[i] = Color.argb(30, Color.red(FILL_COLORS[i]), Color.green(FILL_COLORS[i]), Color.blue(FILL_COLORS[i]));
		}
		POT_NAMES = new String[NAMES.length];
		for(int i = 0; i < NAMES.length; i++){
			POT_NAMES[i] = NAMES[i] + " Lid";
		}
	}
	
	public interface FarmListener {
		public void onNewPotValue(int animalIndex, float x, float y, boolean isCurrentTarget);
		public void onNewState(FarmState state);
		public void onNewTarget(int targetIndex, String animalName);
	}
	
	public static enum FarmState {
		UNKNOWN,
		CHOOSING,
		WAITING,
		SUCCESS,
		FAILURE,
		END
	}
	
	private List<SimpleSeries> potData;
	private List<FarmListener> listeners;
	private FarmState state;
	private int currentTarget;
	
	private long measurementStartTime;
	
	public Farm(){
		potData = new ArrayList<SimpleSeries>(NUM_SINKS);
		listeners = new ArrayList<FarmListener>();
		state = FarmState.UNKNOWN;
		currentTarget = -1;
		for(int i = 0; i < NUM_SINKS; i++){
			final int potI = i;
			potData.add(new SimpleSeries(){
				@Override
				public String getName() {
					return POT_NAMES[potI];
				}
				@Override
				public int getColor() {
					return DATA_COLORS[potI];
				}
				@Override
				public int getFillColor() {
					return FILL_COLORS[potI];
				}
				@Override
				public DataType getXType() {
					return DataType.TIME;
				}
				@Override
				public DataType getYType() {
					return DataType.GENERIC;
				}
				
			});
		}
		
		measurementStartTime = System.currentTimeMillis();
	}
	
	/** Loads a read-only Farm. */
	public Farm(File csvFile) throws IOException{
		this();
		BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile)));
		String line = in.readLine(); // drop first line, we know what it is!
		while((line = in.readLine()) != null){
			String[] lineSplit = line.split(COMMA_SEPARATOR, -1); // limit == -1 implies -> keep trailing empty strings
			Log.d(LOG_TAG, Arrays.deepToString(lineSplit));
			for(int i = 0; i < NUM_SINKS; i++){
				SimpleSeries series = potData.get(i);
				if(!lineSplit[2*i].isEmpty()){
					series.add(Float.parseFloat(lineSplit[2*i]), Float.parseFloat(lineSplit[2*i + 1]));
				}
			}
		}
		in.close();
		state = FarmState.END;
	}
	
	public void addListener(FarmListener listener){
		listeners.add(listener);
	}
	
	public void removeListener(FarmListener listener){
		listeners.remove(listener);
	}
	
	public int getCurrentTarget(){
		return currentTarget;
	}
	
	public String getCurrentTargetName(){
		if(currentTarget < 0) return "No Target";
		return NAMES[currentTarget];
	}
	
	public DataSeries getCurrentTargetSeries(){
		return potData.get(currentTarget);
	}
	
	public DataSeries getData(int animalIndex){
		return potData.get(animalIndex);
	}
	
	private static final String COMMA_SEPARATOR = ",";
	public void writeToCsv(File file) throws IOException{
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
		// write title bar
		int maxDataCount = 0;
		for(int i = 0; i < NUM_SINKS; i++){
			out.write("Timestamp");
			out.write(COMMA_SEPARATOR);
			out.write(POT_NAMES[i]);
			if(i < NUM_SINKS - 1) out.write(COMMA_SEPARATOR);
			
			maxDataCount = Math.max(maxDataCount, potData.get(i).getCount());
		}
		out.newLine();
		
		// write data
		for(int i = 0; i < maxDataCount; i++){
			for(int j = 0; j < NUM_SINKS; j++){
				DataSeries series = potData.get(j);
				if(i < series.getCount()){
					out.write(Float.toString(series.get(i).x));
				}
				out.write(COMMA_SEPARATOR);
				if(i < series.getCount()){
					out.write(Float.toString(series.get(i).y));
				}
				if(j < NUM_SINKS - 1) out.write(COMMA_SEPARATOR);
			}
			out.newLine();
		}
		
		out.close();
	}
	
	/*
	 * CUSTOM PROTOCOL YAY
	 * 
	 * -- PACKET --
	 * Data format:
	 * DATA_HEADER [1 byte, constant]
	 * DATA_LENGTH [1 byte]
	 * DATA        [DATA_LENGTH bytes]
	 * 
	 * -- SEND DATA CONTENTS --
	 * [0] - HELLO
	 * [1] - GET_STATE
	 * [2] - GET_TARGET
	 * 
	 * 
	 * -- RECEIVE DATA CONTENTS--
	 * [0] - HELLO
	 * [1] - STATE
	 * 		length 1
	 * 		[0]
	 * 			0 - CHOOSING
	 * 			1 - WAITING
	 * 			2 - SUCCESS
	 * 			3 - FAILURE
	 * [2] - TARGET
	 * 		length 1
	 * 		[0]
	 * 			0-3
	 * [3] - POT
	 * 		length 2
	 * 		[0] * 256 + [1] - value
	 */
	private static final byte MSG_STATE		= 1;
	private static final byte MSG_TARGET	= 2;
	private static final byte MSG_POT		= 3;
	private static final String LOG_TAG = "Farm";
	
	public void absorbNewMessage(byte[] message){
//		final int length = message[1] & 0xFF;
//		Log.d(LOG_TAG, "Handling message of length " + length);
		switch(message[2]){ // what type?
		case MSG_STATE:
			final int newState = message[3] & 0xFF;
			state = FarmState.values()[newState + 1];
//			Log.d(LOG_TAG, "Switched to state " + state);
			for(int i = 0; i < listeners.size(); i++){
				listeners.get(i).onNewState(state);
			}
			
			break;
		case MSG_TARGET:
			final int newTarget = message[3] & 0xFF;
			currentTarget = newTarget;
//			Log.d(LOG_TAG, "Switched to target " + newTarget);
			for(int i = 0; i < listeners.size(); i++){
				listeners.get(i).onNewTarget(currentTarget, getCurrentTargetName());
			}
			
			break;
		case MSG_POT:
			final int potI = message[3] & 0xFF;
			final int potValue = (message[4] & 0xFF) * 256 + (message[5] & 0xFF);
			if(potI > 3) return; // obscure bug - may be fixed
			
			final float currentTimeSecs = (float)(System.currentTimeMillis() - measurementStartTime) / 1000;
			potData.get(potI).add(currentTimeSecs, potValue);
//			Log.d(LOG_TAG, "Got new pot value " + potValue + " for pot " + potI + ", current target " + currentTarget);
			
			for(int i = 0; i < listeners.size(); i++){
				listeners.get(i).onNewPotValue(potI, currentTimeSecs, potValue, potI == currentTarget);
			}
			
			break;
		}
	}
}
