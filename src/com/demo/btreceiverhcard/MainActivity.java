/*
 * Niccolo' Pignatelli
 * &
 * Justas Medeisis
 * 2014 Imperial College HCARD course
 */

package com.demo.btreceiverhcard;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.demo.btreceiverhcard.R;

/** The application's homescreen. Offers options to connect to the game board or review past measurements. */
public class MainActivity extends Activity{

	private static final String LOG_TAG = "MainActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final Button connectButton = (Button) findViewById(R.id.connectButton);
		final Button reviewButton = (Button) findViewById(R.id.reviewButton);
		
		connectButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Log.d(LOG_TAG, "Starting ConnectActivity.");
				Intent connectIntent = new Intent(MainActivity.this, ConnectActivity.class);
				startActivity(connectIntent);
			}
			
		});
		
		reviewButton.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Log.d(LOG_TAG, "Starting DataListActivity.");
				Intent listDataIntent = new Intent(MainActivity.this, DataListActivity.class);
				startActivity(listDataIntent);
			}
		});
	}
}