package com.demo.btreceiverhcard;

import com.demo.btreceiverhcard.BTConnectionService.BTConnectionBinder;
import com.demo.btreceiverhcard.BTConnectionService.State;
import com.demo.btreceiverhcard.Farm.FarmListener;
import com.demo.btreceiverhcard.Farm.FarmState;
import com.demo.btreceiverhcard.dataview.AutoSeries;
import com.demo.btreceiverhcard.dataview.DataType;
import com.demo.btreceiverhcard.dataview.LineChart;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/** The live data display view. Displays information about the active {@link Farm}'s state and its potentiometer readings. */
public class MeasurementActivity extends ActionBarActivity {

	private static final String LOG_TAG = "MeasurementActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_measurement);

		// Set up the action bar to show a dropdown list.
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		// Show the Up button in the action bar.
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new LineGraphFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.measurement, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true; // TODO offer customization options
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A fragment containing a {@link LineChart}.
	 */
	public static class LineGraphFragment extends Fragment implements ServiceConnection, FarmListener {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		private static final String ARG_SECTION_NUMBER = "section_number";

		private boolean bound = false;
		private BTConnectionService btService;
		
		private TextView statusText;
		private TextView targetText;
		private LineChart graph;
		private AutoSeries activeSeries;
		
		/**
		 * Returns a new instance of this fragment for the given section number.
		 */
		public static LineGraphFragment newInstance(int sectionNumber) {
			LineGraphFragment fragment = new LineGraphFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, sectionNumber);
			fragment.setArguments(args);
			return fragment;
		}

		public LineGraphFragment() {
			
		}
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState){
			super.onActivityCreated(savedInstanceState);
			
			getActivity().getApplicationContext().startService(BTConnectionService.getIntent(getActivity(), new Messenger(new IncomingBTHandler())));
			
			Intent intent = new Intent(getActivity(), BTConnectionService.class);
			getActivity().getApplicationContext().bindService(intent, this, Context.BIND_AUTO_CREATE);
		}
		
		@Override
		public void onDestroy(){
			super.onDestroy();
			// Unbind from our service.
			if(bound){
				getActivity().getApplicationContext().unbindService(this);
				bound = false; // onServiceDisconnected() not guaranteed
				getActivity().getApplicationContext().stopService(new Intent(getActivity(), BTConnectionService.class));
			}
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_measurement, container, false);
			statusText = (TextView) rootView.findViewById(R.id.currentState);
			targetText = (TextView) rootView.findViewById(R.id.currentTarget);
			
			graph = (LineChart) rootView.findViewById(R.id.graph);
			activeSeries = new AutoSeries(graph, "Active Potentiometer", DataType.TIME, DataType.GENERIC);
			graph.addSeries(activeSeries);
			return rootView;
		}

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			BTConnectionBinder binder = (BTConnectionBinder) service;
			btService = binder.getService();
			if(State.CONNECTED == btService.getState()){
				btService.getActiveFarm().addListener(this);
			}
			bound = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName className) {
			Log.e(LOG_TAG, "onServiceDisconnected()");
			bound = false;
		}

		@Override
		public void onNewPotValue(int animalIndex, float x, float y, boolean isCurrentTarget) {
			if(isCurrentTarget){
//				Log.d(LOG_TAG, "Got new pot value!");
				activeSeries.add(x, y);
			}
		}

		@Override
		public void onNewState(final FarmState state) {
			statusText.post(new Runnable(){
				@Override
				public void run() {
					statusText.setText("State: " + state);
				}
			});
		}

		@Override
		public void onNewTarget(int targetIndex, final String animalName) {
			targetText.post(new Runnable(){
				@Override
				public void run() {
					targetText.setText("Target: " + animalName);
				}
			});
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Intent goToMainActivity = new Intent(this, MainActivity.class);
		goToMainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // Will clear out your activity history stack till now
		startActivity(goToMainActivity);

	}

	private static class IncomingBTHandler extends Handler{
		/*TextView serviceState;

		public IncomingBTHandler(TextView serviceState){
			this.serviceState = serviceState;
		}*/

		@Override 
		public void handleMessage(Message msg){
			switch(msg.what){
			case BTConnectionService.MESSAGE_STATE_CHANGE:
//				State newState = (State) msg.obj;
//				serviceState.setText("Service " + newState);
				break;
			case BTConnectionService.MESSAGE_READ:
//				byte[] message = (byte[]) msg.obj;
//				String messageString= new String(message);
//				Log.d(LOG_TAG, "Read " + ByteArrayUtils.getDec(message) + " aka " + ByteArrayUtils.getHex(message) + " as " + messageString);
				break;
			}
		}
	}

}
