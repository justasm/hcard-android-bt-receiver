package com.demo.btreceiverhcard.dataview;

import android.graphics.PointF;

/** An ordered 1-dimensional series of data. Must be sorted by x-coordinate. */
public interface DataSeries {
	public int getCount();
	
	public PointF get(int position);
	public PointF getLatest();
	
	public DataType getXType();
	public DataType getYType();
	
	public String getName();
	public int getColor();
	public int getFillColor();
}
