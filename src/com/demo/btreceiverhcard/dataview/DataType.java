package com.demo.btreceiverhcard.dataview;

/** Denotes a general class of data. Primarily used to select appropriate labels for a {@link DataSeries}. */
public enum DataType {
	/** A generic series, 1 unit = 1 unit. */
	GENERIC{
		private final float[] DIVISOR_BASE_VALUES = {1, 5, 10, 50, 100};

		@Override
		public float getDivisor(float range) {
			// try to pick one of the sane defaults
			for(int i = 1; i < DIVISOR_BASE_VALUES.length; i++){
				if(DIVISOR_BASE_VALUES[i] * MIN_DIVISOR_COUNT > range){
					return DIVISOR_BASE_VALUES[i - 1];
				}
			}

			// if none of the base values work, make it a power of 10
			int divisor = 10;
			while(MIN_DIVISOR_COUNT * divisor < range){
				divisor *= 10;
			}
			return divisor / 10;
		}

		@Override
		public String getLabel(float units) {
			return (int) units + "";
		}

	},

	/** A time series, 1 unit = 1 second. */
	TIME{
		private final float[] DIVISOR_BASE_VALUES = {1, 5, 10, 30, 60, 300, 600, 1800,
				3600, // 1h
				10800, // 3h
				43200, // 12h
				86400, // day
				604800, // week
				2628000, // month
				31540000}; // year 

		@Override
		public float getDivisor(float range) {
			// try to pick one of the sane defaults
			for(int i = 1; i < DIVISOR_BASE_VALUES.length; i++){
				if(DIVISOR_BASE_VALUES[i] * MIN_DIVISOR_COUNT > range){
					return DIVISOR_BASE_VALUES[i - 1];
				}
			}

			// if none of the base values work, return highest
			return DIVISOR_BASE_VALUES[DIVISOR_BASE_VALUES.length - 1];
		}

		@Override
		public String getLabel(float units) {
			final int years = (int) units / 31540000;
			final int months = (int) units / 2628000;
			final int days = (int) units / 86400;
			final int hours = (int) units / 3600;
			final int minutes = ((int) units % 3600) / 60;
			final int seconds = (int) units % 60;
			
			if(years > 0){
				return years + " yr(s)";
			}
			if(months > 0){
				return months + " month(s)";
			}
			if(days > 0){
				return days + " day(s)";
			}
			if(hours > 0){
				return hours + "h";
			}
			if(minutes > 0){
				if(seconds > 0) return minutes + "m" + seconds + "s";
				return minutes + "m";
			}
			return seconds + "s";
		}

	};

	private static final int MIN_DIVISOR_COUNT = 3;

	public abstract float getDivisor(float range);
	public abstract String getLabel(float units);
}