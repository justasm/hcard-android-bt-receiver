/*
 * Viewport calculations based on the official InteractiveChart sample.
 * -------------------------------------------------------------------------
 * 
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.demo.btreceiverhcard.dataview;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View;
import android.widget.OverScroller;

/** A custom line chart view. Add data using {@link #addSeries(DataSeries)}, update using {@link #onAdd(PointF)} or see {@link AutoSeries}. */
public class LineChart extends View implements OnGestureListener, OnDoubleTapListener, OnScaleGestureListener {

	private static float pointRadius;
	private static float density;

	private static final int BG_COLOR = Color.DKGRAY;
	private static final int AXIS_COLOR = Color.GRAY;
	private Paint axisPaint;
	private Paint guidePaint;
	private Paint linePaint;
	private Paint dataPaint;
	private Paint labelPaint;
	private Paint fillPaint;
	private Paint namePaint;

	private Path fillPath;

	private static final float EXTRA_PADDING = 16;
	private static final float GUIDE_WIDTH = 1;
	private static final float AXIS_WIDTH = 2;
	private static final float LINE_WIDTH = 3;
	private static final float POINT_STROKE_WIDTH = 5;
	private static final float TEXT_SIZE_DP = 12;
	private static final float NAME_LABEL_PADDING = 10;

	private final List<DataSeries> series;

	private GestureDetectorCompat gestureDetector;
	private ScaleGestureDetector scaleDetector;

	/** The area in the view designated for displaying the data. In pixel coordinates. */
	private Rect contentBounds = new Rect();
	/**
	 * The area of the data currently visible. In data coordinates.
	 * Note that this rectangle's top is the smaller y value, unlike {@link #dataBounds}.
	 */
	private RectF currentViewport = new RectF();
	/** The domain and range of the data. In data coordinates.
	 * ({@link RectF#top} - range max, {@link RectF#bottom} - range min, {@link RectF#left} - domain min, {@link RectF#right} - domain max)
	 * Note that this means the RectF is flipped vertically, i.e. {@link RectF#height()} is negative.
	 */
	private RectF dataBounds = new RectF();

	private OverScroller scroller;
	/**
	 * This is the active focal point in terms of the viewport.
	 */
	private PointF viewportFocus = new PointF();
	private float lastSpanX;
	private float lastSpanY;
	private RectF scrollerStartViewport = new RectF();
	private Point scrollSurfaceSize = new Point();
	private Zoomer zoomer;
	private PointF zoomFocalPoint = new PointF();
    private static final float ZOOM_AMOUNT = 0.25f;
    private static final boolean VERTICAL_SCALE_DISABLED = true;
    
    /** Trims data points that fall within a short pixel distance of each other. Increases performance at big zoom levels at cost of accuracy. */
    private static final boolean TRIM_SMALL_SCREEN_DELTA_POINTS = true;
    
    private boolean autoscroll = true;
    private boolean animateAutoscroll = false;
    private boolean drawPoints = true;
    private boolean drawName = true;
    
    private int maxSeriesNameHeight;
	private Rect nameBounds = new Rect();
	private float namePadding;
	private float extraPadding;

	public LineChart(Context context, AttributeSet attrs){
		super(context, attrs);

		density = context.getResources().getDisplayMetrics().density;

		pointRadius = 4 * density;
		
		namePadding = NAME_LABEL_PADDING * density;
		extraPadding = EXTRA_PADDING * density;

		initGraphics();

		currentViewport.set(0, 0, 10, 10);
		dataBounds.set(0, 10, 10, 0);

		gestureDetector = new GestureDetectorCompat(context, this);
		gestureDetector.setOnDoubleTapListener(this);
		gestureDetector.setIsLongpressEnabled(false);
		scaleDetector = new ScaleGestureDetector(context, this);
		scroller = new OverScroller(context);
		zoomer = new Zoomer(context);

		series = new ArrayList<DataSeries>(4);
	}

	private void initGraphics(){
		axisPaint = new Paint();
		axisPaint.setColor(AXIS_COLOR);
		axisPaint.setStrokeWidth(density * AXIS_WIDTH);

		guidePaint = new Paint();
		guidePaint.setColor(AXIS_COLOR);
		guidePaint.setStrokeWidth(density * GUIDE_WIDTH);
		guidePaint.setTextSize(density * TEXT_SIZE_DP);

		linePaint = new Paint();
		linePaint.setStrokeWidth(density * LINE_WIDTH);
		linePaint.setAntiAlias(true);

		dataPaint = new Paint();
		dataPaint.setStyle(Style.STROKE);
		dataPaint.setStrokeWidth(density * POINT_STROKE_WIDTH);
		dataPaint.setAntiAlias(true);

		labelPaint = new Paint();
		labelPaint.setColor(AXIS_COLOR);
		labelPaint.setTextSize(density * TEXT_SIZE_DP);
		labelPaint.setTextAlign(Align.CENTER);

		fillPaint = new Paint();
		fillPaint.setStyle(Style.FILL);
		
		namePaint = new Paint();
		namePaint.setColor(Color.WHITE);
		namePaint.setTextSize(density * 16);
		namePaint.setAntiAlias(true);

		fillPath = new Path();
	}

	@Override
	public void onSizeChanged(int w, int h, int oldw, int oldh){
		super.onSizeChanged(w, h, oldw, oldh);
		contentBounds.set(
				getPaddingLeft(),
				getPaddingTop(),
				getWidth() - getPaddingRight(),
				getHeight() - getPaddingBottom());
	}
	
	public boolean removeSeries(DataSeries dataToRemove){
		return series.remove(dataToRemove);
	}
	
	public void removeAllSeries(){
		series.clear();
	}
	
	public void addSeries(DataSeries newData){
		if(series.size() > 0){
			DataSeries existingData = series.get(0);
			if(existingData.getXType() != newData.getXType() || existingData.getYType() != newData.getYType()){
				throw new IllegalArgumentException("Attempted to add new DataSeries of type " + newData.getXType() + " x, " + newData.getYType() +
						" y; must match existing DataSeries type - " + existingData.getXType() + " x, " + existingData.getYType() + " y.");
			}
		}
		series.add(newData);
		for(int i = 0; i < newData.getCount(); i++){
			onAdd(newData.get(i));
		}
		
		namePaint.getTextBounds(newData.getName(), 0, newData.getName().length(), nameBounds);
		final int nameHeight = nameBounds.height();
		maxSeriesNameHeight = Math.max(maxSeriesNameHeight, nameHeight);
	}

	public void onAdd(PointF point){
		dataBounds.bottom = Math.min(dataBounds.bottom, point.y);
		dataBounds.top = Math.max(dataBounds.top, point.y);
		dataBounds.left = Math.min(dataBounds.left, point.x);
		dataBounds.right = Math.max(dataBounds.right, point.x);
		
		if(VERTICAL_SCALE_DISABLED){
			currentViewport.top = Math.min(currentViewport.top, dataBounds.bottom);
			currentViewport.bottom = Math.max(currentViewport.bottom, dataBounds.top);
		}
		
		ViewCompat.postInvalidateOnAnimation(this);
		
		if(autoscroll) scrollToLatestData(animateAutoscroll);
	}
	
	// TODO use elsewhere
	private int dataUnitToPixelX(float x){
		return (int) (contentBounds.width() * (x - dataBounds.left) / currentViewport.width());
	}

	@Override
	public void onDraw(Canvas canvas){
		canvas.drawColor(BG_COLOR);

		final float fullWidth = getWidth();
		final float fullHeight = getHeight();
		
		final float baselineY = getDrawY(0);
		
		// draw horizontal axis
		canvas.drawLine(0, baselineY, fullWidth, baselineY, axisPaint);
		
		if(series.size() < 1) return;
		
		// TODO cache
		final DataType xDataType = series.get(0).getXType();
		final DataType yDataType = series.get(0).getYType();

		// draw horizontal guides
		final float horizontalGuideDivisor = yDataType.getDivisor(currentViewport.height());
		for(int i = 1; i < -dataBounds.height() / horizontalGuideDivisor; i++){
			final float y = getDrawY(i * horizontalGuideDivisor);
			canvas.drawLine(0, y, fullWidth, y, guidePaint);
			canvas.drawText(yDataType.getLabel(i * horizontalGuideDivisor), 2 * density, y - 2 * density, guidePaint);
		}
		
		// draw vertical guides
		final float verticalGuideDivisor = xDataType.getDivisor(currentViewport.width());
		for(int i = 1; i < dataBounds.width() / verticalGuideDivisor; i++){
			final float x = getDrawX(i * verticalGuideDivisor);
			canvas.drawLine(x, 0, x, baselineY, guidePaint);
			canvas.drawText(xDataType.getLabel(i * verticalGuideDivisor), x + 2 * density, fullHeight - 2 * density, guidePaint);
		}

		for(int i = 0; i < series.size(); i++){
			drawSeries(i, canvas, fullWidth, fullHeight, baselineY);
		}
	}
	
	private void drawSeries(int position, Canvas canvas, final float fullWidth, final float fullHeight, final float baselineY){
		DataSeries data = series.get(position);
		final int count = data.getCount();
		if(count < 1) return; // no data to draw..
		

		linePaint.setColor(data.getColor());
		fillPaint.setColor(data.getFillColor());
		
		PointF dataPoint = data.get(0);
		float xPrev = getDrawX(dataPoint.x);
		float yPrev = getDrawY(dataPoint.y);
		for(int i = 1; i < count; i++){
			dataPoint = data.get(i);
			final float x = getDrawX(dataPoint.x);
			final float y = getDrawY(dataPoint.y);
			
			if(x < 0){
				// outside left bounds
				xPrev = x;
				yPrev = y;
				continue;
			}
			if(xPrev > fullWidth){
				// outside right bounds - we assume all subsequent points will be too
				break;
			}
			
			if(TRIM_SMALL_SCREEN_DELTA_POINTS && Math.abs(x - xPrev) < pointRadius && Math.abs(y - yPrev) < pointRadius){
				// points are so close on screen as to be indistinguishable; might as well skip a few to reduce draw calls
				continue;
			}

			fillPath.moveTo(xPrev, baselineY);
			fillPath.lineTo(xPrev, yPrev);
			fillPath.lineTo(x, y);
			fillPath.lineTo(x, baselineY);
			fillPath.close();
			canvas.drawPath(fillPath, fillPaint);
			fillPath.rewind();
			
			canvas.drawLine(xPrev, yPrev, x, y, linePaint);

			if(drawPoints) drawDataPoint(canvas, data.getColor(), xPrev, yPrev);

			xPrev = x;
			yPrev = y;
		}
		if(drawPoints) drawDataPoint(canvas, data.getColor(), xPrev, yPrev);
		
		if(drawName){
			namePaint.getTextBounds(data.getName(), 0, data.getName().length(), nameBounds);
			final int nameWidth = nameBounds.width();
			final float offset = (maxSeriesNameHeight + 2 * namePadding) * position;
			final int origAlpha = fillPaint.getAlpha();
			fillPaint.setAlpha(255);
			canvas.drawRect(
					fullWidth - nameWidth - 2 * namePadding - extraPadding,
					fullHeight - offset - 2 * namePadding - extraPadding - maxSeriesNameHeight,
					fullWidth - extraPadding,
					fullHeight - offset - extraPadding, fillPaint);
			fillPaint.setAlpha(origAlpha);
			canvas.drawText(data.getName(), fullWidth - extraPadding - nameWidth - namePadding, fullHeight - offset - extraPadding - namePadding, namePaint);
		}
	}
	
	private void drawDataPoint(Canvas canvas, int color, float x, float y){
		dataPaint.setStyle(Style.STROKE);
		dataPaint.setColor(color);
		canvas.drawCircle(x, y, pointRadius, dataPaint);
		dataPaint.setStyle(Style.FILL);
		dataPaint.setColor(BG_COLOR);
		canvas.drawCircle(x, y, pointRadius, dataPaint);
	}

	private float getDrawX(final float x){
		return contentBounds.left + contentBounds.width() * (x - currentViewport.left) / currentViewport.width();
	}

	private float getDrawY(final float y){
		return contentBounds.bottom - contentBounds.height() * (y - currentViewport.top) / currentViewport.height();
	}

	@Override
	public int getPaddingLeft(){
		return super.getPaddingLeft() + (int)(2 * density * EXTRA_PADDING);
	}
	@Override
	public int getPaddingRight(){
		return super.getPaddingRight() + (int)(density * EXTRA_PADDING);
	}

	@Override
	public int getPaddingTop(){
		return super.getPaddingTop() + (int)(density * EXTRA_PADDING);
	}
	@Override
	public int getPaddingBottom(){
		return super.getPaddingBottom() + (int)(density * EXTRA_PADDING);
	}
	
	
	/**
	 * CONFIGURATION
	 * ------------------------------------------------------------------------------------------------
	 */
	public void setDrawPointsEnabled(boolean enabled){
		drawPoints = enabled;
	}
	
	public void setAutoscrollEnabled(boolean enabled, boolean animate){
		autoscroll = enabled;
		animateAutoscroll = animate;
	}
	
	public void setSeriesNameEnabled(boolean enabled){
		drawName = enabled;
	}
	
	private void scrollToLatestData(boolean animate){
		scrollerStartViewport.set(currentViewport);
		
		PointF latest = new PointF();
		for(int i = 0; i < series.size(); i++){
			PointF potentialLatest = series.get(i).getLatest();
			if(potentialLatest.x > latest.x) latest = potentialLatest;
		}
		
		int startX = dataUnitToPixelX(scrollerStartViewport.left);
		int dx = dataUnitToPixelX(dataBounds.left + latest.x - scrollerStartViewport.right);
		
		scroller.startScroll(startX, scroller.getCurrY(), dx, scroller.getCurrY(), animate ? 500 : 0);
	}
	
	/**
	 * GESTURES AND TOUCH CONTROL
	 * ------------------------------------------------------------------------------------------------
	 */

	@Override
	public boolean onTouchEvent(MotionEvent event){
		boolean handled = gestureDetector.onTouchEvent(event);
		handled = scaleDetector.onTouchEvent(event) || handled; // note order!
		return handled || super.onTouchEvent(event);
	}

	@Override
	public void computeScroll(){
		super.computeScroll();

		boolean needsInvalidate = false;

		if(scroller.computeScrollOffset()){
			// scroller isn't finished, therefore fling or scroll is in progress
			computeScrollSurfaceSize(scrollSurfaceSize);
			
			int currentX = scroller.getCurrX();
			int currentY = scroller.getCurrY();

			float currXRange = dataBounds.left + (dataBounds.right - dataBounds.left) * currentX / scrollSurfaceSize.x;
			float currYRange = dataBounds.top - (dataBounds.top - dataBounds.bottom) * currentY / scrollSurfaceSize.y;
			setViewportBottomLeft(currXRange, currYRange);
		}

		if(zoomer.computeZoom()){
			// zoom is in progress (either programmatically or via double-touch).
			float newWidth = (1f - zoomer.getCurrZoom()) * scrollerStartViewport.width();
			float newHeight = (1f - zoomer.getCurrZoom()) * scrollerStartViewport.height();
			if(VERTICAL_SCALE_DISABLED) newHeight = scrollerStartViewport.height();
			float pointWithinViewportX = (zoomFocalPoint.x - scrollerStartViewport.left) / scrollerStartViewport.width();
			float pointWithinViewportY = (zoomFocalPoint.y - scrollerStartViewport.top) / scrollerStartViewport.height();
			currentViewport.set(
					zoomFocalPoint.x - newWidth * pointWithinViewportX,
					zoomFocalPoint.y - newHeight * pointWithinViewportY,
					zoomFocalPoint.x + newWidth * (1 - pointWithinViewportX),
					zoomFocalPoint.y + newHeight * (1 - pointWithinViewportY));
			constrainViewport();
			needsInvalidate = true;
		}

		if(needsInvalidate) ViewCompat.postInvalidateOnAnimation(this);
	}

	@Override
	public boolean onDown(MotionEvent e) {
		scrollerStartViewport.set(currentViewport);
		scroller.forceFinished(true);
		ViewCompat.postInvalidateOnAnimation(this);
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		// Flings based on pixel math (not data units)
		computeScrollSurfaceSize(scrollSurfaceSize);
		scrollerStartViewport.set(currentViewport);

		int startX = (int) (scrollSurfaceSize.x * (scrollerStartViewport.left - dataBounds.left) / (dataBounds.right - dataBounds.left));
		int startY = (int) (scrollSurfaceSize.y * (dataBounds.top - scrollerStartViewport.bottom) / (dataBounds.top - dataBounds.bottom));

		scroller.forceFinished(true);
		scroller.fling(
				startX,
				startY,
				-(int) velocityX,
				-(int) velocityY,
				0, scrollSurfaceSize.x - contentBounds.width(),
				0, scrollSurfaceSize.y - contentBounds.height(),
				contentBounds.width() / 2,
				contentBounds.height() / 2);
		ViewCompat.postInvalidateOnAnimation(this);

		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) { }

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		// Scroll based on data unit math (not pixels)
		float viewportOffsetX = distanceX * currentViewport.width() / contentBounds.width();
		float viewportOffsetY = - distanceY * currentViewport.height() / contentBounds.height();

		setViewportBottomLeft(currentViewport.left + viewportOffsetX, currentViewport.bottom + viewportOffsetY);

		return true;
	}

	/**
	 * Calculates the size of the scrollable surface in pixel coordinates at the current viewport scale.
	 * @param out stores the resulting width and height.
	 */
	private void computeScrollSurfaceSize(Point out){
		out.set((int) (contentBounds.width() * (dataBounds.right - dataBounds.left) / currentViewport.width()),
				(int) (contentBounds.height() * (dataBounds.top - dataBounds.bottom) / currentViewport.height()));
	}

	/**
	 * Sets the current viewport to the given X and Y positions.
	 * Note that the Y value represents the topmost pixel position, and thus
	 * the bottom of the {@link #currentViewport} rectangle. For more details on why top and
	 * bottom are flipped, see {@link #currentViewport}.
	 */
	private void setViewportBottomLeft(float x, float y){
		/**
		 * Constrains within the scroll range. The scroll range is simply the viewport extremes
		 * (AXIS_X_MAX, etc.) minus the viewport size. For example, if the extrema were 0 and 10,
		 * and the viewport size was 2, the scroll range would be 0 to 8.
		 */
		float currentWidth = currentViewport.width();
		float currentHeight = currentViewport.height();

		x = Math.max(dataBounds.left, Math.min(x, dataBounds.right - currentWidth));
		y = Math.max(dataBounds.bottom + currentHeight, Math.min(y, dataBounds.top));

		currentViewport.set(x, y - currentHeight, x + currentWidth, y);
		ViewCompat.postInvalidateOnAnimation(this);
	}

	@Override
	public void onShowPress(MotionEvent e) { }

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return true;
	}

	@Override
	public boolean onScaleBegin(ScaleGestureDetector detector) {
		lastSpanX = ScaleGestureDetectorCompat.getCurrentSpanX(detector);
		lastSpanY = ScaleGestureDetectorCompat.getCurrentSpanY(detector);
		return true;
	}

	@Override
	public boolean onScale(ScaleGestureDetector detector) {
		float spanX = ScaleGestureDetectorCompat.getCurrentSpanX(detector);
		float spanY = ScaleGestureDetectorCompat.getCurrentSpanY(detector);

		float newWidth = lastSpanX / spanX * currentViewport.width();
		float newHeight = VERTICAL_SCALE_DISABLED ? currentViewport.height() : lastSpanY / spanY * currentViewport.height();

		float focusX = detector.getFocusX();
		float focusY = detector.getFocusY();
		hitTest(focusX, focusY, viewportFocus);

		currentViewport.set(
				viewportFocus.x - newWidth * (focusX - contentBounds.left) / contentBounds.width(),
				viewportFocus.y - newHeight * (contentBounds.bottom - focusY) / contentBounds.height(),
				0,
				0);
		currentViewport.right = currentViewport.left + newWidth;
		currentViewport.bottom = currentViewport.top + newHeight;
		constrainViewport();
		ViewCompat.postInvalidateOnAnimation(this);

		lastSpanX = spanX;
		lastSpanY = spanY;
		return true;
	}

	@Override
	public void onScaleEnd(ScaleGestureDetector detector) {
		ViewCompat.postInvalidateOnAnimation(this);
	}

	/**
	 * Finds the chart point (i.e. within the chart's domain and range) represented by the
	 * given pixel coordinates, if that pixel is within the chart region described by
	 * {@link #contentBounds}. If the point is found, the "dest" argument is set to the point and
	 * this function returns true. Otherwise, this function returns false and "dest" is unchanged.
	 */
	private boolean hitTest(float x, float y, PointF dest) {
		if (!contentBounds.contains((int) x, (int) y)) {
			return false;
		}

		dest.set(
				currentViewport.left
				+ currentViewport.width()
				* (x - contentBounds.left) / contentBounds.width(),
				currentViewport.top
				+ currentViewport.height()
				* (y - contentBounds.bottom) / -contentBounds.height());
		return true;
	}

	/** Ensures the viewport is within {@link #dataBounds}. */
	private void constrainViewport(){
		currentViewport.left = Math.max(dataBounds.left, currentViewport.left);
		currentViewport.top = Math.max(dataBounds.bottom, currentViewport.top);
		currentViewport.bottom = Math.max(Math.nextUp(currentViewport.top), Math.min(dataBounds.top, currentViewport.bottom));
		currentViewport.right = Math.max(Math.nextUp(currentViewport.left), Math.min(dataBounds.right, currentViewport.right));
	}

	@Override
	public boolean onDoubleTap(MotionEvent e) {
		zoomer.forceFinished(true);
		if (hitTest(e.getX(), e.getY(), zoomFocalPoint)) {
			zoomer.startZoom(ZOOM_AMOUNT);
		}
		ViewCompat.postInvalidateOnAnimation(this);
		return true;
	}

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		return false;
	}
}
