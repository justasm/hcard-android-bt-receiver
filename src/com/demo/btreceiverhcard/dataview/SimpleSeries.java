package com.demo.btreceiverhcard.dataview;

import java.util.ArrayList;
import java.util.List;

import android.graphics.PointF;

/** A {@link DataSeries} which assumes points are added with {@link #add(float, float)} in sequential order with increasing x. Stores the {@link PointF}s in a list. */
public abstract class SimpleSeries implements DataSeries {

	private List<PointF> data;
	
	public SimpleSeries(){
		data = new ArrayList<PointF>();
	}
	
	public PointF add(float x, float y){
		PointF newPoint = new PointF(x, y);
		data.add(newPoint);
		return newPoint;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public PointF get(int position) {
		return data.get(position);
	}

	@Override
	public PointF getLatest() {
		return data.get(data.size() - 1);
	}

}
