package com.demo.btreceiverhcard.dataview;

import java.lang.ref.WeakReference;

import android.graphics.Color;
import android.graphics.PointF;

/** A {@link SimpleSeries} that maintains a reference to a {@link LineChart} and automatically updates it upon {@link #add(float, float)}. */
public class AutoSeries extends SimpleSeries {

	private static final int DATA_COLOR = Color.parseColor("#00CC00");
	private static final int DATA_FILL_COLOR = Color.parseColor("#2200CC00");
	
	private final String name;
	private WeakReference<LineChart> chartRef;
	
	private final DataType xDataType;
	private final DataType yDataType;
	
	public AutoSeries(LineChart chart, String name, DataType xDataType, DataType yDataType){
		this.name = name;
		this.xDataType = xDataType;
		this.yDataType = yDataType;
		chartRef = new WeakReference<LineChart>(chart);
	}
	
	@Override
	public PointF add(float x, float y){
		PointF newPoint = super.add(x, y);
		LineChart chart = chartRef.get();
		if(null != chart){
			chart.onAdd(newPoint);
		}
		return newPoint;
	}
	
	@Override
	public DataType getXType() {
		return xDataType;
	}

	@Override
	public DataType getYType() {
		return yDataType;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getColor() {
		return DATA_COLOR;
	}

	@Override
	public int getFillColor() {
		return DATA_FILL_COLOR;
	}
	
}
