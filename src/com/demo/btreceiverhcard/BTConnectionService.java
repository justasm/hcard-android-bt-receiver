package com.demo.btreceiverhcard;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.text.format.Time;
import android.util.Log;

/**
 * The primary background service which establishes and maintains the connection to the game board.
 * Expects received message packets according to the custom protocol as outline in the comment to {@link #DATA_HEADER}.
 */
public class BTConnectionService extends Service {

	private static final String LOG_TAG = "BTConnectionService";

	private static final String MESSENGER_EXTRA = "messenger";

	/** Standard SerialPortService ID. */
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;

	private static final int ONGOING_NOTIFICATION_ID = 1;
	private final IBinder binder = new BTConnectionBinder();

	/**
	 * Custom Bluetooth message packet protocol.<p>
	 * Data format:
	 * <li>DATA_HEADER		[1 byte, constant == 0xDA]
	 * <li>DATA_LENGTH		[1 byte]
	 * <li>DATA				[DATA_LENGTH bytes]
	 */
	private static final byte DATA_HEADER = (byte) 0xDA;

	private BluetoothAdapter btAdapter;
	private ConnectThread connectThread;
	private ConnectedThread connectedThread;

	private List<Messenger> messengers;
	
	private Farm activeFarm;

	public static enum State {
		DISCONNECTED,
		CONNECTING,
		CONNECTED;
	}
	private State state;

	public static Intent getIntent(Context client, Messenger messenger){
		Intent intent = new Intent(client, BTConnectionService.class);
		intent.putExtra(MESSENGER_EXTRA, messenger);
		return intent;
	}

	public class BTConnectionBinder extends Binder {
		/** @return this service's instance, so clients can call public methods. */
		BTConnectionService getService(){
			return BTConnectionService.this;
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(){
		super.onCreate();
		
		state = State.DISCONNECTED;
		
		messengers = new ArrayList<Messenger>();
		
		btAdapter = BluetoothAdapter.getDefaultAdapter();

		// Start the foreground notification
		Intent notificationIntent = new Intent(this, MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		Notification notification = new Notification.Builder(this)
							.setContentTitle("BT Connection Service")
							.setContentText("Touch to manage.")
							.setSmallIcon(R.drawable.cow)
							.setContentIntent(pendingIntent)
							.getNotification();
		startForeground(ONGOING_NOTIFICATION_ID, notification);
	}
	
	public void unregisterListener(Messenger messenger){
		messengers.remove(messenger);
	}
	
	/** Notifies any Context that supplied a Messenger with the Intent starting this service. */
	private void notifyListeners(Message msg){
		for(int i = 0; i < messengers.size(); i++){
			try {
				messengers.get(i).send(i == 0? msg : Message.obtain(msg)); // TODO figure out whether making copies is necessary?
			} catch (RemoteException e) {
				Log.e(LOG_TAG, "Failed to send message to " + messengers.get(i) + ", deleting.", e);
				messengers.remove(i);
				--i;
			}
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		Bundle extras = intent.getExtras();
		if(null != extras && extras.containsKey(MESSENGER_EXTRA)){
			messengers.add((Messenger) extras.getParcelable(MESSENGER_EXTRA));
		}

		return START_NOT_STICKY; // don't recreate if killed!
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
		
		// Store the active farm session to disk
		if(null != activeFarm){
			if(isExternalStorageWritable()){
				File dir = getDataFileStorageDir("HCARD");
				Time time = new Time();
				time.setToNow();
				try {
					activeFarm.writeToCsv(new File(dir, "FARM " + time.format3339(false) + ".csv"));
				} catch (IOException e) {
					Log.e(LOG_TAG, "Failed to write to file.", e);
				}
			}
		}

		// Clean up all threads
		if (connectThread != null) {connectThread.cancel(); connectThread = null;}
		if (connectedThread != null) {connectedThread.cancel(); connectedThread = null;}
	}
	
	/* Checks if external storage is available for read and write */
	public static boolean isExternalStorageWritable() {
		String state = Environment.getExternalStorageState();
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			return true;
		}
		return false;
	}
	
	public File getDataFileStorageDir(String dataName){
		File file = getFile(dataName);
		if(!file.mkdirs() && !file.isDirectory()){
			Log.e(LOG_TAG, "Directory " + dataName + " not created.");
		}
		return file;
	}
	
	@TargetApi(Build.VERSION_CODES.KITKAT)
	private File getFile(String dataName){
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
			return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), dataName);
		} else {
			return new File(getExternalFilesDir(null), dataName);
		}
	}

	/**
	 * Set the current state of the connection.
	 */
	private synchronized void setState(State newState) {
		Log.d(LOG_TAG, "setState() " + state + " -> " + newState);
		state = newState;

		// Notify any listening activity of the state change.
		notifyListeners(Message.obtain(null, MESSAGE_STATE_CHANGE, newState));
	}

	/** @return the current connection state. */
	public synchronized State getState() {
		return state;
	}

	public synchronized void connect(BluetoothDevice device) {
		// Cancel any thread attempting to make a connection
		if (State.CONNECTING == state) {
			if (connectThread != null) {connectThread.cancel(); connectThread = null;}
		}

		// Cancel any thread currently running a connection
		if (connectedThread != null) {connectedThread.cancel(); connectedThread = null;}

		// Start the thread to connect with the given device
		connectThread = new ConnectThread(device);
		connectThread.start();
		setState(State.CONNECTING);
	}

	/**
	 * Starts the ConnectedThread to begin managing a Bluetooth connection.
	 * @param socket  BluetoothSocket on which the connection was made
	 * @param device  BluetoothDevice that has been connected
	 */
	private synchronized void onConnectionSuccess(BluetoothSocket socket, BluetoothDevice device) {
		// Cancel the thread that completed the connection
		if (connectThread != null) {connectThread.cancel(); connectThread = null;}

		// Cancel any thread currently running a connection
		if (connectedThread != null) {connectedThread.cancel(); connectedThread = null;}

		// Start the thread to manage the connection and perform transmissions
		connectedThread = new ConnectedThread(socket);
		connectedThread.start();
		
		// Create a new Farm object to handle received data
		activeFarm = new Farm();
		
		setState(State.CONNECTED);
	}
	
	public Farm getActiveFarm(){
		return activeFarm;
	}

	/**
	 * Indicates that the connection attempt failed.
	 */
	private void onConnectionFail() {
		// TODO handle better..
		
		setState(State.DISCONNECTED);
	}

	/**
	 * Indicates that the connection was lost.
	 */
	private void onConnectionLost() {
		// TODO handle better..
		
		setState(State.DISCONNECTED);
	}

	/**
	 * Write to the ConnectedThread in an unsynchronized manner
	 * @param out The bytes to write
	 * @see ConnectedThread#write(byte[])
	 */
	public void write(byte[] out) {
		// Create temporary object
		ConnectedThread r;
		// Synchronize a copy of the ConnectedThread
		synchronized (this) {
			if (State.CONNECTED != state) return;
			r = connectedThread;
		}
		// Perform the write unsynchronized
		r.write(out);
	}

	/**
	 * This thread runs while attempting to make an outgoing connection
	 * with a device. It runs straight through; the connection either
	 * succeeds or fails.
	 */
	private class ConnectThread extends Thread {
		private final BluetoothSocket socket;
		private final BluetoothDevice device;

		public ConnectThread(BluetoothDevice device) {
			this.device = device;
			BluetoothSocket tmp = null;

			// Get a BluetoothSocket for a connection with the given BluetoothDevice
			try {
				tmp = device.createRfcommSocketToServiceRecord(MY_UUID);
			} catch (IOException e) {
				Log.e(LOG_TAG, "Failed to create RFCOMM socket.", e);
			}
			socket = tmp;
		}

		public void run() {
			Log.i(LOG_TAG, "BEGIN mConnectThread Socket");
			setName("ConnectThread");

			// Always cancel discovery because it will slow down a connection
			btAdapter.cancelDiscovery();

			// Make a connection to the BluetoothSocket
			try {
				// This is a blocking call and will only return on a
				// successful connection or an exception
				socket.connect();
			} catch (IOException e) {
				// Close the socket
				try {
					socket.close();
				} catch (IOException e2) {
					Log.e(LOG_TAG, "unable to close() socket during connection failure", e2);
				}
				onConnectionFail();
				return;
			}

			// Reset the ConnectThread because we're done
			synchronized (BTConnectionService.this) {
				connectThread = null;
			}

			// Start the connected thread
			onConnectionSuccess(socket, device);
		}

		public void cancel() {
			try {
				socket.close();
			} catch (IOException e) {
				Log.e(LOG_TAG, "close() of socket failed", e);
			}
		}
	}

	/**
	 * This thread runs during a connection with the remote device.
	 * It handles all incoming and outgoing transmissions.
	 */
	private class ConnectedThread extends Thread {
		private final BluetoothSocket socket;
		private final InputStream inStream;
		private final OutputStream outStream;

		public ConnectedThread(BluetoothSocket socket) {
			this.socket = socket;
			InputStream tmpIn = null;
			OutputStream tmpOut = null;

			// Get the input and output streams, using temp objects because
			// member streams are final
			try {
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			} catch (IOException e) {
				e.printStackTrace();
			}

			inStream = tmpIn;
			outStream = tmpOut;
		}

		public void run() {
			byte[] tempBuffer = new byte[1024];  // buffer store for the stream

			byte[] messageBuffer = new byte[512];
			boolean messageInProgress = false;
			int readMessagePosition = 0;
			int readMessageLimit = 512;

			Log.d("ConnectedThread", "Started running.");

			// Keep listening to the InputStream until an exception occurs
			while (true) {
				try {
					// Read from the InputStream
					int numBytesRead = inStream.read(tempBuffer); // blocks until we actually get something

					byte[] logArray = new byte[numBytesRead];
					System.arraycopy(tempBuffer, 0, logArray, 0, numBytesRead);
//					Log.d("ConnectedThread", "Read " + ByteArrayUtils.getHex(logArray));

					for(int i = 0; i < numBytesRead; i++){
						byte b = tempBuffer[i];

						if(messageInProgress){
//							Log.d("ConnectedThread", "Message in progress, received byte " + b + " for position " + readMessagePosition);

							// In process of reading message, put into buffer
							messageBuffer[readMessagePosition++] = b;

							if(readMessagePosition >= readMessageLimit){
								// We're done with one message!
//								Log.d("ConnectedThread", "MESSAGE DONE");

								// Make copy of our message so we can send it off into the world
								byte[] messageRead = new byte[readMessagePosition];
								System.arraycopy(messageBuffer, 0, messageRead, 0, messageRead.length);
								
								// Handle message locally
								activeFarm.absorbNewMessage(messageRead);
								
								// Send the obtained bytes to any listeners
								notifyListeners(Message.obtain(null, MESSAGE_READ, messageRead));

								// Reset all values
								readMessagePosition = 0;
								readMessageLimit = 512;
								messageInProgress = false;
							} else if(readMessagePosition == 2){
								// Second byte will indicate our message length
								readMessageLimit = 1 + 1 + (b & 0xFF); // header byte + length byte + data
								
//								Log.d("ConnectedThread", "MESSAGE LIMIT " + readMessageLimit);
							}
						} else if(DATA_HEADER == b){
//							Log.d("ConnectedThread", "NEW MESSAGE byte " + b);
							// We're starting a new message!
							messageInProgress = true;

							// Store header in message as well
							messageBuffer[readMessagePosition++] = b;
						} else {
							Log.i("ConnectedThread", "Dropping byte " + b);
							// Not part of our message or our header, thus drop it!
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
					onConnectionLost();
					break;
				}
			}
		}

		/* Call this from the main activity to send data to the remote device */
		public void write(byte[] bytes) {
			try {
				outStream.write(bytes);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/* Call this from the main activity to shutdown the connection */
		public void cancel() {
			try {
				socket.close();
			} catch (IOException e) { }
		}
	}
}
