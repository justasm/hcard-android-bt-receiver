package com.demo.btreceiverhcard;

public class ByteArrayUtils {
	/** Converts a byte array to a decimal string representation. */
	public static String getDec(byte[] bytes){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
	    for (byte b : bytes) {
	        sb.append((b & 0xFF) + ", ");
	    }
	    sb.delete(sb.length()-2, sb.length());
		sb.append("]");
	    return sb.toString();
	}
	
	/** Converts a byte array to a hex string representation. */
	public static String getHex(byte[] bytes){
		StringBuilder sb = new StringBuilder();
		sb.append("[");
	    for (byte b : bytes) {
	        sb.append("0x" + String.format("%02X, ", b));
	    }
	    sb.delete(sb.length()-2, sb.length());
		sb.append("]");
	    return sb.toString();
	}
}
